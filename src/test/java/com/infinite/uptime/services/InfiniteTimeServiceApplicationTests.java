package com.infinite.uptime.services;



import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

import org.hibernate.service.spi.ServiceException;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import com.infinite.uptime.exceptions.BusinessException;
import com.infinite.uptime.model.MachineServiceDeatails;
import com.infinite.uptime.model.ServiceReqMachineDetails;
import com.infinite.uptime.model.ServiceRequest;
import com.infinite.uptime.persistence.AttachmentRepository;
import com.infinite.uptime.persistence.AttachmentSerReqRepository;
import com.infinite.uptime.persistence.MachineServiceDeatailsRepository;
import com.infinite.uptime.persistence.ServiceReqMachineDetailsRepository;
import com.infinite.uptime.persistence.ServiceRequestRepository;
import com.infinite.uptime.resource.ServiceCreationResource;
import com.infinite.uptime.service.AvailableUserService;



/*
 * package com.infinite.uptime;
 * 
 * import org.junit.jupiter.api.Test; import
 * org.springframework.boot.test.context.SpringBootTest;
 * 
 * @SpringBootTest class InfiniteTimeServiceApplicationTests {
 * 
 * @Test void contextLoads() { }
 * 
 * }
 */

class InfiniteTimeServiceApplicationTests {
	
//	@Test
//    public void f() {
//    }
// 
//    @BeforeTest
//    public void beforeTest() {
//    }
// 
//    @AfterTest
//    public void afterTest() {
//    }
//    
//    @Test
//	public void oneMoreTest() {
//		System.out.println("This is a TestNG-Maven based test");
//	}
	
	private AvailableUserService availableUserService;
	private ServiceCreationResource serviceCreationResource;
    private ServiceRequestRepository mockServiceRequestRepository;
    private ServiceReqMachineDetailsRepository mockserviceReqMachineDetailsRepository;
    private MachineServiceDeatailsRepository mockmachineServiceDeatailsRepository;
//    private ServiceReqMachineDetailsRepository mockServiceReqMachineDetailsRepository;
//    private MachineServiceDeatailsRepository mockMachineServiceDeatailsRepository;
//    private AttachmentSerReqRepository mockAttachmentSerReqRepository;
//    private AttachmentRepository mockAttachmentRepository;
    
    @BeforeClass
    public void setUp() throws Exception  {
    	availableUserService = new AvailableUserService();
    	mockServiceRequestRepository = mock (ServiceRequestRepository.class);
    	mockserviceReqMachineDetailsRepository = mock (ServiceReqMachineDetailsRepository.class);
    	mockmachineServiceDeatailsRepository = mock (MachineServiceDeatailsRepository.class);
    	  ReflectionTestUtils.setField(availableUserService, "serviceRequestRepository", mockServiceRequestRepository);
    	//  ReflectionTestUtils.setField(serviceCreationResource, "serviceRequestRepository", mockServiceRequestRepository);
    	  ReflectionTestUtils.setField(availableUserService, "serviceReqMachineDetailsRepository", mockserviceReqMachineDetailsRepository);
    	  ReflectionTestUtils.setField(availableUserService, "machineServiceDeatailsRepository", mockmachineServiceDeatailsRepository);	  
   	  
    }	
	
}