
package com.infinite.uptime.interceptor;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class ServiceInterceptor implements HandlerInterceptor {

	/*
	 * @Override public ClientHttpResponse intercept(HttpRequest request, byte[]
	 * body, ClientHttpRequestExecution execution) throws IOException {
	 * 
	 * ClientHttpResponse response = execution.execute(request, body); //
	 * response.getHeaders().add("Foo", "bar"); return response; }
	 */ 
	
	/*
	 * @Override public boolean preHandle(HttpServletRequest request,
	 * HttpServletResponse response, Object handler) throws Exception { final String
	 * uri = "http://localhost:8080/springrestexample/employees"; RestTemplate
	 * restTemplate = new RestTemplate();
	 * 
	 * HttpHeaders headers = new HttpHeaders();
	 * headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	 * //headers.set("Authorization", request.get); HttpEntity<String> entity = new
	 * HttpEntity<String>(headers); ResponseEntity<String> authResponse =
	 * restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
	 * 
	 * if (authResponse.getStatusCode().value() != 200) { //sthrow new }
	 * 
	 * return true; }
	 */
	  @Override public void postHandle(HttpServletRequest request,
	  HttpServletResponse response, Object handler, ModelAndView modelAndView)
	  throws Exception {
	  
	  System.out.println("Inside the Post Handle method"); }
	  
	  @Override public void afterCompletion (HttpServletRequest request,
	  HttpServletResponse response, Object handler, Exception exception) throws
	  Exception {
	  
	  System.out.println("After completion of request and response");
	  
	  }
	  
	 	
	
	
	
}