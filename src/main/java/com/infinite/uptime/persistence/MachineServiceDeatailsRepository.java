package com.infinite.uptime.persistence;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.infinite.uptime.model.MachineServiceDeatails;
import com.infinite.uptime.model.ServiceReqMachineDetails;

@Repository
@Transactional
public interface MachineServiceDeatailsRepository extends CrudRepository<MachineServiceDeatails, Long>{

}
