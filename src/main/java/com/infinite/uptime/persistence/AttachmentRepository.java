package com.infinite.uptime.persistence;

import org.springframework.data.repository.CrudRepository;

import com.infinite.uptime.model.Attachment;

public interface AttachmentRepository  extends CrudRepository<Attachment, Long> {

}
