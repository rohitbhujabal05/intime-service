package com.infinite.uptime.persistence;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.infinite.uptime.model.AttachmentSerReq;
import com.infinite.uptime.model.MachineServiceDeatails;

@Repository
@Transactional
public interface AttachmentSerReqRepository  extends CrudRepository<AttachmentSerReq, Long>{

}
