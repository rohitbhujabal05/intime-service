package com.infinite.uptime.persistence;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.infinite.uptime.model.ServiceReqMachineDetails;
import com.infinite.uptime.model.ServiceRequest;

@Repository
@Transactional
public interface ServiceReqMachineDetailsRepository extends CrudRepository<ServiceReqMachineDetails, Long> {

	ServiceReqMachineDetails findByMachineId(Long machineId);

	List<ServiceReqMachineDetails> findByMonitorIdAndPlantId(int monitorId, Long plantId);

	List<ServiceReqMachineDetails> findByMonitorIdAndMachineName(int monitorId, String machineName);

	List<ServiceReqMachineDetails> findByMonitorIdAndPlantIdAndMachineName(int monitorId, Long plantId,
			String machineName);

}
