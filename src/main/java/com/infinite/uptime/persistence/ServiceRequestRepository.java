package com.infinite.uptime.persistence;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.infinite.uptime.model.ServiceReqMachineDetails;
import com.infinite.uptime.model.ServiceRequest;

@Repository
@Transactional
public interface ServiceRequestRepository extends CrudRepository<ServiceRequest, String>{

	List<ServiceRequest> findByUserId(final Long userId);

    @Query(
        value = "SELECT * FROM SERVICE_REQ SR JOIN SERVICE_REQ_MACHINE_DETAILS AS SRMD ON SR.ID=SRMD.SERVICE_REQ_ID WHERE SRMD.MACHINE_ID LIKE %:machineId% and MACHINE_NAME LIKE %:machineName%",
        nativeQuery = true)
	List<ServiceRequest> findByMachineId(@Param("machineId") String machineId,@Param("machineName") String machineName);

    @Query(
            value = "SELECT * FROM SERVICE_REQ SR JOIN SERVICE_REQ_MACHINE_DETAILS AS SRMD ON SR.ID=SRMD.SERVICE_REQ_ID WHERE SRMD.MACHINE_ID=:machineId and SR.USER_ID=:userId",
            nativeQuery = true)
	List<ServiceRequest> findByMachineIdAndUserId(@Param("machineId") Long machineId, @Param("userId") Long userId);

    @Query(
            value = "SELECT * FROM SERVICE_REQ  WHERE ID LIKE %:serviceRequestId%",
            nativeQuery = true)
	ServiceRequest  findByIdContainingIgnoreCase(@Param("serviceRequestId")  String serviceRequestId);
}
