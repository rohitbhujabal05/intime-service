package com.infinite.uptime.service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.infinite.uptime.model.MachineServiceDeatails;
import com.infinite.uptime.model.ServiceReqMachineDetails;
import com.infinite.uptime.model.ServiceRequest;
import com.infinite.uptime.model.request.AvailableUserRequest;
import com.infinite.uptime.model.request.AvailableUserUpdateRequest;
import com.infinite.uptime.model.request.MachineDetailsRequest;
import com.infinite.uptime.model.request.MachineDetailsUpdateRequest;
import com.infinite.uptime.model.request.ServiceMachineRequest;
import com.infinite.uptime.model.response.AvailableUserResponse;
import com.infinite.uptime.persistence.AttachmentRepository;
import com.infinite.uptime.persistence.AttachmentSerReqRepository;
import com.infinite.uptime.persistence.MachineServiceDeatailsRepository;
import com.infinite.uptime.persistence.ServiceReqMachineDetailsRepository;
import com.infinite.uptime.exceptions.FileStorageException;
import com.infinite.uptime.exceptions.UserNotFoundException;
import com.infinite.uptime.logger.JLogManager;
import com.infinite.uptime.exceptions.BusinessException;
import com.infinite.uptime.exceptions.FileNotFoundException;
import com.infinite.uptime.model.Attachment;
import com.infinite.uptime.model.AttachmentSerReq;
import com.infinite.uptime.model.FileStorageProperties;
import com.infinite.uptime.persistence.ServiceRequestRepository;
import com.infinite.uptime.resource.ServiceCreationResource;
import com.infinite.uptime.util.IErrConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class AvailableUserService {

	@Autowired
	private ServiceRequestRepository serviceRequestRepository;

	@Autowired
	private AttachmentRepository attachmentRepository;

	@Autowired
	private AttachmentSerReqRepository attachmentSerReqRepository;

	@Autowired
	ServiceReqMachineDetailsRepository serviceReqMachineDetailsRepository;

	@Autowired
	MachineServiceDeatailsRepository machineServiceDeatailsRepository;

	private Path fileStorageLocation;

	private final ObjectMapper objectMapper;
	
	private static final Logger logger = LoggerFactory.getLogger(AvailableUserService.class);

	
	private final String CLASS_NAME = "AvailableUserService";

	public AvailableUserService() {
		this.fileStorageLocation = null;
		objectMapper = new ObjectMapper();
		final JavaTimeModule javaTimeModule = new JavaTimeModule();
		final LocalDateTimeDeserializer localDateTimeDeserializer = new LocalDateTimeDeserializer(
				DateTimeFormatter.ISO_DATE_TIME);
		javaTimeModule.addDeserializer(LocalDateTime.class, localDateTimeDeserializer);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.registerModule(javaTimeModule);
	}

	public ServiceRequest createAvailableUser(AvailableUserRequest availableUserRequest) {

		ServiceRequest serviceRequest = new ServiceRequest();
		serviceRequest.setId(ServiceRequest.generateRandomDigits(9));
		serviceRequest.setUserId(availableUserRequest.getUserId());
		serviceRequest.setStatus("REQUEST_SUBMITTED");
		serviceRequest.setCreatedDate(LocalDateTime.now(ZoneOffset.UTC));
		ServiceRequest savedServiceRequest = serviceRequestRepository.save(serviceRequest);

		List<MachineServiceDeatails> machineServiceDeatails = new ArrayList<MachineServiceDeatails>();
		List<ServiceReqMachineDetails> serviceReqMachineDetails = new ArrayList<ServiceReqMachineDetails>();
		for (MachineDetailsRequest machineDetailsRequest : availableUserRequest.getMachineDetailsRequest()) {

			ServiceReqMachineDetails serviceReqMachineMapping = new ServiceReqMachineDetails();
			serviceReqMachineMapping.setServiceRequest(savedServiceRequest);
			serviceReqMachineMapping.setMachineId(machineDetailsRequest.getMachineId());
			serviceReqMachineMapping.setMachineName(machineDetailsRequest.getMachineName());
			serviceReqMachineMapping.setPlantId(machineDetailsRequest.getPlantId());
			serviceReqMachineMapping.setOrgId(machineDetailsRequest.getOrgId());
			serviceReqMachineMapping.setFault(machineDetailsRequest.getFault());
			serviceReqMachineMapping.setRecommendation(machineDetailsRequest.getRecommendation());
			serviceReqMachineMapping.setHealthScore(machineDetailsRequest.getHealthScore());
			serviceReqMachineMapping.setAlarms(machineDetailsRequest.getAlarms());
			serviceReqMachineMapping.setAppointmentDate(machineDetailsRequest.getAppointmentDate());
			serviceReqMachineMapping.setDeviceIdentifier(machineDetailsRequest.getDeviceIdentifier());
			serviceReqMachineMapping.setTriggeredDiagnostics(machineDetailsRequest.getTriggeredDiagnostics());
			serviceReqMachineMapping.setSince(machineDetailsRequest.getSince());
			serviceReqMachineMapping.setStatus(machineDetailsRequest.getStatus());
			serviceReqMachineMapping.setMonitorName(machineDetailsRequest.getMonitorName());
			serviceReqMachineMapping.setMonitorId(machineDetailsRequest.getMonitorId());
			serviceReqMachineMapping.setMachineGroupName(machineDetailsRequest.getMachineGroupName());
			serviceReqMachineMapping.setSpeed(machineDetailsRequest.getSpeed());
			serviceReqMachineMapping.setSubscriptionEnd(machineDetailsRequest.getSubscriptionEnd());
			serviceReqMachineMapping.setSubscriptionStart(machineDetailsRequest.getSubscriptionStart());
			serviceReqMachineMapping.setCreatedDate(LocalDateTime.now(ZoneOffset.UTC));
			serviceReqMachineMapping.setComment(machineDetailsRequest.getComment());

			ServiceReqMachineDetails saved = serviceReqMachineDetailsRepository.save(serviceReqMachineMapping);

			serviceReqMachineDetailsRepository.save(serviceReqMachineMapping);
			for (ServiceMachineRequest serviceMachineRequest : machineDetailsRequest.getServiceRequest()) {
				MachineServiceDeatails machineServiceDeatail = new MachineServiceDeatails();
				machineServiceDeatail.setVendorId(serviceMachineRequest.getVendor());
				machineServiceDeatail.setServiceName(serviceMachineRequest.getServiceName());
				machineServiceDeatail.setServiceReqMachineDetails(saved);
				machineServiceDeatail.setServiceId(1L);
				// machineServiceDeatail.setServiceId(serviceMachineRequest.getServiceName());
				machineServiceDeatailsRepository.save(machineServiceDeatail);
				MachineServiceDeatails MachineServiceDeatails = machineServiceDeatailsRepository
						.save(machineServiceDeatail);
				machineServiceDeatails.add(MachineServiceDeatails);
			}
		}
		serviceRequest.setServiceReqMachineDetails(serviceReqMachineDetails);
		return serviceRequestRepository.save(serviceRequest);
	}

	public ServiceRequest listAvaibleServices(String serviceRequestId) throws BusinessException {
		ServiceRequest serviceRequest = serviceRequestRepository.findByIdContainingIgnoreCase(serviceRequestId);
		if(serviceRequest==null) {
			throw new BusinessException(IErrConstants.IUT000, "Id not found", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return serviceRequest;
	}

	public List<ServiceRequest> listAvaibleServicesByUserId(Long userId) throws BusinessException {
		// List<ServiceRequest> serviceRequest
		// =serviceRequestRepository.findByUserIdAndCreatedDateDesc(userId);
		List<ServiceRequest> serviceRequest = serviceRequestRepository.findByUserId(userId);
		if(serviceRequest==null) {
			throw new BusinessException(IErrConstants.IUT000, "Id not found", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return serviceRequest;
	}

	public List<ServiceRequest> listGeneratedServiceByMachineId(String machineId, String machineName) throws BusinessException {
		List<ServiceRequest> serviceRequests = serviceRequestRepository.findByMachineId(machineId, machineName);
		if(serviceRequests==null) {
			throw new BusinessException(IErrConstants.IUT000, "Id not found", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		serviceRequests.forEach(serviceRequest -> serviceRequest.getServiceReqMachineDetails()
				.removeIf(s -> (!s.getMachineId().equalsIgnoreCase(machineId))));

		return serviceRequests;

	}

	public List<ServiceRequest> listGeneratedServiceByMachineIdAndUserId(Long userId, Long machineId) throws BusinessException {
		List<ServiceRequest> serviceReqMachineDetails = serviceRequestRepository.findByMachineIdAndUserId(machineId,
				userId);
		if(serviceReqMachineDetails==null) {

			throw new BusinessException(IErrConstants.IUT000, "Id not found", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return serviceReqMachineDetails;

	}

	public ServiceRequest updateGeneratedServiceRequest(String serviceId, AvailableUserUpdateRequest serviceRequest) throws BusinessException {
		final ServiceRequest serviceRequestdata = listAvaibleServices(serviceId);
		if(serviceRequestdata==null) {
			throw new UserNotFoundException("Id not found" +"serviceId: "+  serviceId); 
			
		}
		if (serviceRequestdata != null) {
			if (serviceRequest.getStatus() != null) {
				serviceRequestdata.setStatus(serviceRequest.getStatus());
				serviceRequestdata.setLastModifiedDate(LocalDateTime.now(ZoneOffset.UTC));
			}

			if (!serviceRequest.getMachineDetailsRequest().isEmpty()) {
				for (ServiceReqMachineDetails object1 : serviceRequestdata.getServiceReqMachineDetails()) {
					for (MachineDetailsUpdateRequest object2 : serviceRequest.getMachineDetailsRequest()) {
						System.out.println(object1.getId());
						System.out.println(object2.getId());
						if (object1.getId() == object2.getId()) {
							object1.setAppointmentDate(object2.getAppointmentDate());
							object1.setSince(object2.getSince());
							object1.setStatus(object2.getStatus());
							object1.setSpeed(object2.getSpeed());
							object1.setSubscriptionEnd(object2.getSubscriptionEnd());
							object1.setSubscriptionStart(object2.getSubscriptionStart());
							object1.setComment(object2.getComment());
							serviceReqMachineDetailsRepository.save(object1);
						}

					}
				}
			}
		}
		return serviceRequestRepository.save(serviceRequestdata);
	}

	public ServiceRequest updateGeneratedMachineServiceRequest(String serviceId,
			AvailableUserUpdateRequest serviceRequest) throws BusinessException {
		final ServiceRequest serviceRequestdata = listAvaibleServices(serviceId);
		if(serviceRequestdata==null) {
			throw new BusinessException(IErrConstants.IUT000, "Id not found", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (serviceRequestdata != null) {
			if (serviceRequest.getStatus() != null) {
				serviceRequestdata.setStatus(serviceRequest.getStatus());
				serviceRequestdata.setLastModifiedDate(LocalDateTime.now(ZoneOffset.UTC));
			}

			if (!serviceRequest.getMachineDetailsRequest().isEmpty()) {
				for (ServiceReqMachineDetails object1 : serviceRequestdata.getServiceReqMachineDetails()) {
					for (MachineDetailsUpdateRequest object2 : serviceRequest.getMachineDetailsRequest()) {
						if (object1.getId() == object2.getId()) {
							object1.setAppointmentDate(object2.getAppointmentDate());
							object1.setSince(object2.getSince());
							object1.setStatus(object2.getStatus());
							object1.setSpeed(object2.getSpeed());
							object1.setSubscriptionEnd(object2.getSubscriptionEnd());
							object1.setSubscriptionStart(object2.getSubscriptionStart());
							object1.setAppointmentDate(object2.getAppointmentDate());
							serviceReqMachineDetailsRepository.save(object1);
						}

					}
				}
			}
		}
		return serviceRequestRepository.save(serviceRequestdata);
	}

	public String storeFile(MultipartFile file) {
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				// throw new FileStorageException("Sorry! Filename contains invalid path
				// sequence " + fileName);
			}

			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

			return fileName;
		} catch (IOException ex) {
			// throw new FileStorageException("Could not store file " + fileName + ". Please
			// try again!", ex);
		}
		return fileName;
	}

	private String generateFileName(final MultipartFile multiPart) {
		return new Date().getTime() + "-" + multiPart.getOriginalFilename().replace(" ", "_");
	}

	private File convertMultiPartToFile(final MultipartFile file) throws IOException {
		final File convFile = new File(file.getOriginalFilename());
		final FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	public Attachment createAttachment(final boolean important, final MultipartFile file, String serviceRequestId) throws BusinessException {
		// validateAttachmentRequest(file);
		
		long fileSizeInBytes = file.getSize();
		long fileSizeInKB = fileSizeInBytes / 1024;
		long fileSizeInMB = fileSizeInKB / 1024;
	 
		if(fileSizeInMB>10) {
			throw new BusinessException(IErrConstants.IUT000, "file not accept more than 10 mb", HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		final String uploadedFileUrl = uploadFile(file);
		Attachment a = attachmentRepository.save(buildModel(important, file, uploadedFileUrl, serviceRequestId));

		attachmentSerReqRepository.save(new AttachmentSerReq(a.getId(), serviceRequestId));
		return a;
	}

	private Attachment buildModel(final boolean important, final MultipartFile file, final String uploadedFileUrl,
			final String serviceRequestId) {
		final Attachment attachment = new Attachment();
		attachment.setFileName(file.getOriginalFilename());
		// attachment.setServiceRequest(listAvaibleServices(serviceRequestId));
		attachment.setFilePath(uploadedFileUrl);
		attachment.setSize(file.getSize());
		attachment.setType(getFileExtension(file.getOriginalFilename()));
		attachment.setCreated(LocalDateTime.now(ZoneOffset.UTC));
		attachment.setUpdated(LocalDateTime.now(ZoneOffset.UTC));
		return attachment;
	}

	private String getFileExtension(final String name) {
		if (StringUtils.isEmpty(name)) {
			return null;
		}
		final int lastIndexOf = name.lastIndexOf('.');
		if (lastIndexOf == -1) {
			return null;
		}
		return name.substring(lastIndexOf + 1);
	}

	@Async
	public String uploadFile(final MultipartFile multipartFile) {
		if (multipartFile == null) {
			return null;
		}
		String fileName = "";
		try {
			fileName = generateFileName(multipartFile);
			final File file = convertMultiPartToFile(multipartFile);
			// uploadFileTos3bucket(fileName, file);
			uploadFileToFolder(fileName, multipartFile);
			// log.info("File upload is completed.");
			file.delete(); // To remove the file locally created in the project folder.
		} catch (final Exception ex) {
			// log.error("Failed to upload. Cause: {}", ex.getMessage());
		}
		return fileName;
	}

	private void uploadFileToFolder(String fileName, MultipartFile file) {

		String fileBasePath = "../InfiniteTime-service/src/main/resources/Attachment/";
		Path path = Paths.get(fileBasePath + fileName);
		try {
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/files/download/")
				.path(fileName).toUriString();

	}



	@Autowired
	public void FileStorageService(FileStorageProperties fileStorageProperties) throws BusinessException {
		String fileBasePath = "../InfiniteTime-service/src/main/resources/Attachment/";
		this.fileStorageLocation = Paths.get(fileBasePath).toAbsolutePath().normalize();

		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			throw new BusinessException(IErrConstants.IUT000, "Could not create the directory where the uploaded files will be stored", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public Resource loadFileAsResource(String fileName) throws FileNotFoundException, MalformedURLException, BusinessException {
		
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (!resource.exists()) {
				
				throw new BusinessException(IErrConstants.IUT000, "File not", HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return resource;
	}

	public void deleteAll(String fileName) throws FileNotFoundException, BusinessException {
		String fileBasePath = "../InfiniteTime-service/src/main/resources/Attachment/";

		File file = new File(fileBasePath + fileName);
		if (!file.exists()) {
			throw new BusinessException(IErrConstants.IUT000, "File not", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		FileSystemUtils.deleteRecursively(Paths.get(fileBasePath + fileName).toFile());

	}

	public List<ServiceReqMachineDetails> listGeneratedServiceByMonitorIdAndPlantId(int monitorId, Long plantId) throws BusinessException {
		 
	List<ServiceReqMachineDetails>serviceReqMachineDetails=serviceReqMachineDetailsRepository.findByMonitorIdAndPlantId(monitorId, plantId);
		
		if(serviceReqMachineDetails.isEmpty()) {
			throw new BusinessException(IErrConstants.IUT000, "Id not found", HttpStatus.INTERNAL_SERVER_ERROR); 

		}
		return serviceReqMachineDetails;
	}

	public List<ServiceReqMachineDetails> listGeneratedServiceByMonitorIdAndPlantIdAndMachineName(int monitorId,
			Long plantId, String machineName) throws BusinessException {

		List<ServiceReqMachineDetails>serviceReqMachineDetails=serviceReqMachineDetailsRepository.findByMonitorIdAndPlantIdAndMachineName(monitorId, plantId,
				machineName);
		if(serviceReqMachineDetails.isEmpty()) {
			
			throw new BusinessException(IErrConstants.IUT000, "Id not found", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return serviceReqMachineDetails;
	}
//	public Attachment createAttachments(boolean b, MultipartFile[] file, String serviceRequestId) {
//		final String uploadedFileUrl = uploadFiles(file);
//		return null;
//	}
	
	
//	@Async
//	public String uploadFiles(final MultipartFile[] multipartFile) {
//		if (multipartFile == null) {
//			return null;
//		}
//		String fileName = "";
//		try {
//			fileName = generateFileNames(multipartFile);
//	//		final File file = convertMultiPartToFile(multipartFile);
//			// uploadFileTos3bucket(fileName, file);
////			uploadFileToFolder(fileName, multipartFile);
//			// log.info("File upload is completed.");
//	//		file.delete(); // To remove the file locally created in the project folder.
//		} catch (final Exception ex) {
//			// log.error("Failed to upload. Cause: {}", ex.getMessage());
//		}
//		return fileName;
//	}

//	private String generateFileNames(MultipartFile[] multiPart) {
//		System.out.println(multiPart);
//		Files[] file = null;
//		for (MultipartFile i:multiPart) {
//			 file= new Date().getTime() + "-" + i.getOriginalFilename().replace(" ", "_");
//		    }
//		System.out.println(file);
//		
//		return file;
//	//	return new Date().getTime() + "-" + multiPart.getOriginalFilename().replace(" ", "_");
//	
//	}
}
