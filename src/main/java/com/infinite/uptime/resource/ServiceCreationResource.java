package com.infinite.uptime.resource;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.infinite.uptime.exceptions.BusinessException;
import com.infinite.uptime.exceptions.FileNotFoundException;
import com.infinite.uptime.exceptions.FileStorageException;
import com.infinite.uptime.logger.JLogManager;
import com.infinite.uptime.model.Attachment;
import com.infinite.uptime.model.ServiceReqMachineDetails;
import com.infinite.uptime.model.ServiceRequest;
import com.infinite.uptime.model.request.AvailableUserRequest;
import com.infinite.uptime.model.request.AvailableUserUpdateRequest;
import com.infinite.uptime.model.response.AvailableUserResponse;
import com.infinite.uptime.service.AvailableUserService;
import com.infinite.uptime.util.IErrConstants;

@RestController
public class ServiceCreationResource {

	private static final Logger logger = LoggerFactory.getLogger(ServiceCreationResource.class);
	/** The class name. */
	private final String CLASS_NAME = "ServiceCreationResource";
	@Autowired
	private AvailableUserService availableUserService;

	private final ObjectMapper objectMapper;

	public ServiceCreationResource() {
		objectMapper = new ObjectMapper();
		final JavaTimeModule javaTimeModule = new JavaTimeModule();
		final LocalDateTimeDeserializer localDateTimeDeserializer = new LocalDateTimeDeserializer(
				DateTimeFormatter.ISO_DATE_TIME);
		javaTimeModule.addDeserializer(LocalDateTime.class, localDateTimeDeserializer);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.registerModule(javaTimeModule);
	}

	@PostMapping(value = "/api/generate/service", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<AvailableUserResponse> GenerateServiceRequest(
			@RequestBody final AvailableUserRequest availableUserRequest) throws BusinessException {
		final String METHOD_NAME = "GenerateServiceRequest"; // GenerateServiceRequest
		ServiceRequest availableUserResponse = null;
		try {
			long startTime = System.currentTimeMillis();
			JLogManager.writeToTrace(CLASS_NAME, METHOD_NAME, "debugging");
			availableUserResponse = availableUserService.createAvailableUser(availableUserRequest);
			return new ResponseEntity<>(objectMapper.convertValue(availableUserResponse, AvailableUserResponse.class),
					HttpStatus.OK);
		} catch (Exception ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME, "Unable to parse  " + availableUserResponse + "]\n" + ex);
			throw new BusinessException(IErrConstants.IUT000, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/api/generate/service/{serviceRequestId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ServiceRequest> listAvailableUserServiceByServiceId(
			@PathVariable("serviceRequestId") final String serviceRequestId) throws BusinessException {
		final String METHOD_NAME = "listAvailableUserServiceByServiceId";
		try {
			ServiceRequest serviceRequest = availableUserService.listAvaibleServices(serviceRequestId);
			// final AvailableUserResponse activitySettingHorseResponse =
			// objectMapper.convertValue(serviceRequest, AvailableUserResponse.class);
			JLogManager.writeToInfo(CLASS_NAME, METHOD_NAME, "debugging");
			return new ResponseEntity<>(serviceRequest, HttpStatus.OK);
		} catch (Exception ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME, "Unable to parse  " + serviceRequestId + "]\n" + ex);
			throw new BusinessException(IErrConstants.IUT000, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/api/generated/service/history", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<List<ServiceRequest>> listGeneratedServiceByUserId(
			@RequestParam(value = "userId") Long userId) throws BusinessException {
		final String METHOD_NAME = "listAvailableUserServiceByUserId";
		try {
			List<ServiceRequest> serviceRequest = availableUserService.listAvaibleServicesByUserId(userId);
			/*
			 * final AvailableUserResponse activitySettingHorseResponse =
			 * objectMapper.convertValue(serviceRequest, AvailableUserResponse.class);
			 */
			JLogManager.writeToInfo(CLASS_NAME, METHOD_NAME, "debugging");
			return new ResponseEntity<>(serviceRequest, HttpStatus.OK);
		} catch (Exception ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME, "Unable to parse  " + userId + "]\n" + ex);
			throw new BusinessException(IErrConstants.IUT000, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/api/generated/service/machinBased", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<List<ServiceRequest>> listGeneratedServiceByMachineId(
			@RequestParam(value = "machineId") String machineId,
			@RequestParam(value = "machineName") String machineName) throws BusinessException {
		// machineId
		final String METHOD_NAME = "listAvailableUserServiceByUserId";
		try {
			List<ServiceRequest> serviceRequests = availableUserService.listGeneratedServiceByMachineId(machineId,
					machineName);
			/*
			 * final List<AvailableUserRequest> activitySettingHorseResponseList =
			 * serviceRequests.stream() .map(serviceRequest ->
			 * objectMapper.convertValue(serviceRequest, AvailableUserRequest.class))
			 * .collect(Collectors.toList());
			 */
			JLogManager.writeToInfo(CLASS_NAME, METHOD_NAME, "debugging");
			return new ResponseEntity<>(serviceRequests, HttpStatus.OK);
		} catch (Exception ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME, "Unable to parse  " + machineId + "]\n" + ex);
			throw new BusinessException(IErrConstants.IUT000, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/api/generated/service/timeLine", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<List<ServiceRequest>> listGeneratedServiceByMachineIdAndUserId(
			@RequestParam(value = "userId") Long userId, @RequestParam(value = "machineId") Long machineId)
			throws BusinessException {

		final String METHOD_NAME = "listGeneratedServiceByMachineIdAndUserId";
		try {
			List<ServiceRequest> serviceRequest = availableUserService.listGeneratedServiceByMachineIdAndUserId(userId,
					machineId);
			/*
			 * final AvailableUserResponse activitySettingHorseResponse =
			 * objectMapper.convertValue(serviceRequest, AvailableUserResponse.class);
			 */

			JLogManager.writeToInfo(CLASS_NAME, METHOD_NAME, "debugging");
			return new ResponseEntity<>(serviceRequest, HttpStatus.OK);
		} catch (Exception ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME, "Unable to parse  " + userId + "]\n" + ex);
			throw new BusinessException(IErrConstants.IUT000, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping
	public String isAlive() {
		return "Yes";
	}

	@PutMapping(value = "/api/generated/service/{ServiceId}")
	@ResponseBody
	public ResponseEntity<ServiceRequest> updateGeneratedServiceRequest(
			@PathVariable("ServiceId") final String ServiceId,
			@RequestBody final AvailableUserUpdateRequest ServiceRequest) throws BusinessException {
		final String METHOD_NAME = "updateGeneratedServiceRequest";
		try {
			final ServiceRequest serviceRequestData = availableUserService.updateGeneratedServiceRequest(ServiceId,
					ServiceRequest);
			JLogManager.writeToInfo(CLASS_NAME, METHOD_NAME, "debugging");
			return new ResponseEntity<>(serviceRequestData, HttpStatus.OK);
		} catch (Exception ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME, "Not found ServiceId Or Null  " + ServiceId + "]\n" + ex);
			throw new BusinessException(IErrConstants.IUT000, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	

	@PutMapping(value = "/api/generated/machinService{ServiceId}")
	@ResponseBody
	public ResponseEntity<ServiceRequest> updateGeneratedMachineServiceRequest(
			@PathVariable("ServiceId") final String ServiceId,
			@RequestBody final AvailableUserUpdateRequest ServiceRequest) throws BusinessException {
		final String METHOD_NAME = "updateGeneratedServiceRequest";
		try {
			final ServiceRequest serviceRequestData = availableUserService
					.updateGeneratedMachineServiceRequest(ServiceId, ServiceRequest);
			JLogManager.writeToInfo(CLASS_NAME, METHOD_NAME, "debugging");
			return new ResponseEntity<>(serviceRequestData, HttpStatus.OK);
		} catch (Exception ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME, " service Id Not Found  " + ServiceId + "]\n" + ex);
			throw new BusinessException(IErrConstants.IUT000, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/downloadFile/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request)
			throws Exception {
		// Load file as Resource
		Resource resource = availableUserService.loadFileAsResource(fileName);
		final String METHOD_NAME = "downloadFile";
		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME, "file could not determinde  " + fileName + "\n" + ex);
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		JLogManager.writeToInfo(CLASS_NAME, METHOD_NAME, "debugging");
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	@DeleteMapping(value = "/api/service/deleteFile/{fileName:.+}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<File> delete(@PathVariable String fileName) throws FileNotFoundException, BusinessException {
		availableUserService.deleteAll(fileName);
		final String METHOD_NAME = "delete ";
		try {
			File file = new File(fileName);
			availableUserService.deleteAll(fileName);
			JLogManager.writeToInfo(CLASS_NAME, METHOD_NAME, "debugging ");
			return new ResponseEntity<>(file, HttpStatus.OK);
		} catch (Exception ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME, " file is not Found " + fileName + "]\n" + ex);
			throw new BusinessException(IErrConstants.IUT000, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/api/generated/services", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<List<ServiceReqMachineDetails>> listGeneratedServiceByMonitorIdAndPlantId(
			@RequestParam(value = "monitorId") int monitorId, @RequestParam(value = "plantId") Long plantId)
			throws BusinessException {
		final String METHOD_NAME = "listAvailableUserServiceBymonitorIdAndplantId";
		try {
			List<ServiceReqMachineDetails> serviceRequests = availableUserService
					.listGeneratedServiceByMonitorIdAndPlantId(monitorId, plantId);
			JLogManager.writeToInfo(CLASS_NAME, METHOD_NAME, " debugging ");
			return new ResponseEntity<>(serviceRequests, HttpStatus.OK);
		} catch (Exception ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME,
					" Id is not Found " + monitorId + "" + plantId + "]\n" + ex);
			throw new BusinessException(IErrConstants.IUT000, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/api/generated/services/bymonitoridplantId", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<List<ServiceReqMachineDetails>> listGeneratedServiceByMonitorIdAndPlantIdAndMachineName(
			@RequestParam(value = "monitorId") int monitorId, @RequestParam(value = "plantId") Long plantId,
			@RequestParam(value = "machineName") String machineName) throws BusinessException {

		final String METHOD_NAME = "listAvailableUserServiceBymonitorIdAndplantIdAndMachineName";
		try {
			List<ServiceReqMachineDetails> serviceRequests = availableUserService
					.listGeneratedServiceByMonitorIdAndPlantIdAndMachineName(monitorId, plantId, machineName);
			JLogManager.writeToInfo(CLASS_NAME, METHOD_NAME, "debugging ");
			return new ResponseEntity<>(serviceRequests, HttpStatus.OK);
		} catch (Exception ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME,
					"Id is not Found" + monitorId + "" + plantId + "" + machineName + "]\n" + ex);
			throw new BusinessException(IErrConstants.IUT000, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value = "/api/service/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Attachment> uploadToLocalFileSystem(@RequestParam("file") MultipartFile file,
			@RequestParam final String serviceRequestId) throws BusinessException {
		final String METHOD_NAME = "uploadToLocalFileSystem";
		try {

			Attachment attachment = availableUserService.createAttachment(true, file, serviceRequestId);
			JLogManager.writeToInfo(CLASS_NAME, METHOD_NAME, "debugging");
			return new ResponseEntity<>(attachment, HttpStatus.OK);
		} catch (Exception ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME, " file not accept  " + file + "]\n" + ex);
			throw new BusinessException(IErrConstants.IUT000, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/api/service/multiplefileupload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Attachment>> uploadToLocalMultipleFileSystem(
			@RequestParam("files") MultipartFile[] files, @RequestParam final String serviceRequestId)
			throws BusinessException {
		final String METHOD_NAME = "uploadToLocalMultipleFileSystem";
		try {
			List<Attachment> attachment = new ArrayList<Attachment>();
			Attachment attachments = null;
			int fileNo = files.length;

			if (fileNo > 6) {
				throw new BusinessException(IErrConstants.IUT000, "file less than 5", HttpStatus.INTERNAL_SERVER_ERROR);
			}
			for (MultipartFile file : files) {
				attachments = availableUserService.createAttachment(true, file, serviceRequestId);
				attachment.add(attachments);
			}
			JLogManager.writeToInfo(CLASS_NAME, METHOD_NAME, "debugging");
			return new ResponseEntity<>(attachment, HttpStatus.OK);
		} catch (Exception ex) {
			JLogManager.writeToAudit(CLASS_NAME, METHOD_NAME, " file not accept  " + files + "]\n" + ex);
			throw new BusinessException(IErrConstants.IUT000, ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
