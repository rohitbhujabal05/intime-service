package com.infinite.uptime.exceptions;

import org.springframework.http.HttpStatus;

import com.infinite.uptime.exceptions.AppException;

public class BusinessException extends AppException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor call.
	 */
	public BusinessException(){
		super();
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param message the message
	 */
	public BusinessException(String message){
		super(message);
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param code the error code
	 * @param args the error message placeholder arguments
	 */
	public BusinessException(String code, String[] args){
		super(code, args);
		this.errorCode = code;
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param code the error code
	 * @param message the error message
	 * @param httpStatus the http status
	 */
	public BusinessException(String code, String message, HttpStatus httpStatus) {
		super(message);
		this.errorCode = code;
		this.httpStatus = httpStatus;
	}
	
	/**
	 * Instantiates a new business exception.
	 *
	 * @param code the code
	 * @param message the message
	 * @param reqRefNumber the req ref number
	 * @param httpStatus the http status
	 */
	public BusinessException(String code, String message, String reqRefNumber, HttpStatus httpStatus) {
		super(message);
		this.errorCode = code;
		this.reqRefNumber = reqRefNumber;
		this.httpStatus = httpStatus;
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param code the error code
	 * @param args the error message placeholder arguments
	 * @param httpStatus the http status
	 */
	public BusinessException(String code, String[] args, HttpStatus httpStatus) {
		super(code, args);
		this.errorCode = code;
		this.httpStatus = httpStatus;
	}
	
	/**
	 * Instantiates a new business exception.
	 *
	 * @param code the code
	 * @param args the args
	 * @param reqRefNumber the req ref number
	 * @param httpStatus the http status
	 */
	/**
	 * @param code
	 * @param args
	 * @param reqRefNumber
	 * @param httpStatus
	 */
	public BusinessException(String code, String[] args, String reqRefNumber, HttpStatus httpStatus) {
		super(code, args);
		this.errorCode = code;
		this.reqRefNumber = reqRefNumber;
		this.httpStatus = httpStatus;
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param ex the exception
	 * @param code the error code
	 * @param args the error message placeholder arguments
	 */
	public BusinessException(Throwable ex, String code, String[] args){
		super(ex, code, args);
		this.errorCode = code;
	}
}
