package com.infinite.uptime.exceptions;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.Properties;

import org.springframework.stereotype.Component;
import com.infinite.uptime.logger.JLogManager;


@Component
public class ErrorMessagesImpl implements ErrorMessages {
	
	public final String CLASS_NAME = "ErrorMessagesImpl";

	/** The prop. */
	private Properties prop;

	/**
	 * Constructor calls initialization method.
	 */
	public ErrorMessagesImpl(){

		init();

	}

	/**
	 * This method initializes Error message with help of ErrorMessagesImpl.properties.
	 */
	private void init(){

		prop = new Properties();
		InputStream input = null;

		try {

			input = ErrorMessagesImpl.class.getClassLoader().getResourceAsStream("ErrorMessages.properties");

			// load a properties file
			if(input != null) {
			   prop.load(input);
			}

		} catch (IOException ex) {
			JLogManager.writeToAudit(CLASS_NAME, "init", ex);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.getMessage();
				}
			}
		}
	}

	/**
	 * This method accepts message code, argument list and returns message.
	 *
	 * @param code the code
	 * @param args the argument list
	 * @return error message
	 */
	@Override
	public String getMessage(String code, String[] args){

		String message = prop.getProperty(code);
		if (message != null){
			MessageFormat mf = new MessageFormat(message);
			message = mf.format(args);
		} else
			message = code;

		return message;
	}

	/**
	 * This method accepts Throwable, message code, argument list and returns message.
	 *
	 * @param ex the exception
	 * @param code the code
	 * @param args the argument list
	 * @return error message
	 */
	@Override
	public String getMessage(Throwable ex, String code, String[] args){

		StringWriter sw = new StringWriter();

		String message = prop.getProperty(code);

		if (message != null){
			MessageFormat mf = new MessageFormat(message);
			message = mf.format(args);

			message = message + "\n" + sw.toString();	// stack trace as a string
		}else
			message = code + "\n" + sw.toString();	// stack trace as a string

		return message;
	}
}
