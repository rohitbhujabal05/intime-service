package com.infinite.uptime.exceptions;

import org.springframework.http.HttpStatus;
import com.infinite.uptime.exceptions.AppException;
import com.infinite.uptime.logger.JLogManager;

public class CriticalException extends AppException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new critical exception.
	 */
	public CriticalException(){
		super();
	}

	/**
	 * Instantiates a new critical exception.
	 *
	 * @param message the error message
	 */
	public CriticalException(String message){
		super(message);
	}

	/**
	 * Instantiates a new critical exception.
	 *
	 * @param code the error code
	 * @param args the error message placeholder arguments
	 */
	public CriticalException(String code, String[] args){
		super(code, args);
		this.errorCode = code;
	}

	/**
	 * Instantiates a new critical exception.
	 *
	 * @param code the error code
	 * @param message the error message
	 * @param httpStatus the http status
	 */
	public CriticalException(String code, String message, HttpStatus httpStatus) {
		super(message);
		this.errorCode = code;
		this.httpStatus = httpStatus;
	}

	/**
	 * Instantiates a new critical exception.
	 *
	 * @param code the error code
	 * @param args the error message placeholder arguments
	 * @param httpStatus the http status
	 */
	public CriticalException(String code, String[] args, HttpStatus httpStatus) {
		super(code, args);
		this.errorCode = code;
		this.httpStatus = httpStatus;
	}

	/**
	 * Instantiates a new critical exception.
	 *
	 * @param code the code
	 * @param args the args
	 * @param reqRefNumber the req ref number
	 * @param httpStatus the http status
	 */
	public CriticalException(String code, String[] args, String reqRefNumber, HttpStatus httpStatus) {
		super(code, args);
		this.errorCode = code;
		this.reqRefNumber = reqRefNumber;
		this.httpStatus = httpStatus;
	}
	
	/**
	 * Instantiates a new critical exception.
	 *
	 * @param ex the exception
	 * @param code the error code
	 * @param args the error message placeholder arguments
	 */
	public CriticalException(Throwable ex, String code, String[] args) {
		super(ex, code, args);
		this.errorCode = code;
	}
}
