package com.infinite.uptime.exceptions;

import org.springframework.http.HttpStatus;

public class AppException  extends Exception{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The err msg. */
	private static ErrorMessages errMsg = new ErrorMessagesImpl();

	/** The error code. */
	protected String errorCode;

	/** The http status. */
	protected HttpStatus httpStatus;
	
	/** The req ref number. */
	protected String reqRefNumber;
	
	/** The user name. */
	protected String userName;
	
	/** The url. */
	protected String url;

	/**
	 * Constructor call.
	 */
	public AppException(){
		super();
	}

	/**
	 * This Constructor accepts message and passes to super class.
	 *
	 * @param message the message
	 */
	public AppException(String message){
		super(message);
	}

	/**
	 * This Constructor accepts message code, argument list and passes to super class.
	 *
	 * @param code the code
	 * @param args the args
	 */
	public AppException(String code, String[] args){
		super(errMsg.getMessage(code, args));
	}

	/**
	 * This Constructor accepts Throwable, message code, argument list and passes to super class.
	 *
	 * @param ex the ex
	 * @param code the code
	 * @param args the args
	 */
	public AppException(Throwable ex, String code, String[] args){
		super(errMsg.getMessage(ex, code, args));
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the error code.
	 *
	 * @param errorCode the new error code
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Gets the http status.
	 *
	 * @return the http status
	 */
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	/**
	 * Sets the http status.
	 *
	 * @param httpStatus the new http status
	 */
	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	/**
	 * Gets the req ref number.
	 *
	 * @return the req ref number
	 */
	public String getReqRefNumber() {
		return reqRefNumber;
	}

	/**
	 * Sets the req ref number.
	 *
	 * @param reqRefNumber the new req ref number
	 */
	public void setReqRefNumber(String reqRefNumber) {
		this.reqRefNumber = reqRefNumber;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

}
