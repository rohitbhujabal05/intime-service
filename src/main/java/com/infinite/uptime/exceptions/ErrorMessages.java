package com.infinite.uptime.exceptions;

public interface ErrorMessages {
	/**
	 * This method accepts message code, argument list and returns message.
	 *
	 * @param code the code
	 * @param args the argument list
	 * @return error message
	 */
	public String getMessage(String code, String[] args);

	/**
	 * This method accepts Throwable, message code, argument list and returns message.
	 *
	 * @param ex the exception
	 * @param code the code
	 * @param args the argument list
	 * @return error message
	 */
	public String getMessage(Throwable ex, String code, String[] args);


}
