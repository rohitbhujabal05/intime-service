package com.infinite.uptime.model.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.infinite.uptime.model.request.MachineDetailsRequest;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(Include.NON_EMPTY)
public class AvailableUserResponse {
 	
 private String id;
 //private Long userId;
 private List<MachineDetailsRequest> machineDetailsRequest;
}
