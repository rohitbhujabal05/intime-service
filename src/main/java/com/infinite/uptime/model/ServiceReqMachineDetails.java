package com.infinite.uptime.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "SERVICE_REQ_MACHINE_DETAILS")
@JsonInclude(Include.NON_EMPTY)
public class ServiceReqMachineDetails implements Serializable {

	private static final long serialVersionUID = 8967608776094849928L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "MACHINE_ID", nullable = false)
	private String machineId;

	@Column(name = "MACHINE_NAME", nullable = false)
	private String machineName;

	@Column(name = "PLANT_ID", nullable = false)
	private Long plantId;

	@Column(name = "ORG_ID") // , nullable = false)
	private Long orgId;

	@Column(name = "FAULT")
	private String fault;

	@Column(name = "RECOMMENDATION")
	private String recommendation;

	@Column(name = "HEALTH_SCORE") // , nullable = false)
	private Double healthScore;

	@Column(name = "APPOINTMENT_DATE") // , nullable = false)
	private Timestamp appointmentDate;

	@Column(name = "SERVICE_DATE") // , nullable = false)
	private LocalDateTime serviceDate;

	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "SERVICE_REQ_ID", nullable = false)
	private ServiceRequest serviceRequest;

	@JsonManagedReference
	@OneToMany(mappedBy = "serviceReqMachineDetails", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MachineServiceDeatails> machineServiceDeatails;

	@Column(name = "CREATED_DATE")
	private LocalDateTime createdDate;

	@Column(name = "SUBSCRIPTION_START")
	private Timestamp subscriptionStart;

	@Column(name = "SUBSCRIPTION_END")
	private Timestamp subscriptionEnd;

	@Column(name = "SPEED")
	private int speed;

	@Column(name = "MACHINE_GROUP_NAME")
	private String machineGroupName;

	@Column(name = "MONITOR_ID")
	private int monitorId;

	@Column(name = "MONITOR_NAME")
	private String monitorName;

	@Column(name = "STATUS")
	private int status;

	@Column(name = "SINCE")
	private String since;

	@Column(name = "ALARMS")
	private Long alarms;

	@Column(name = "TRIGGERED_DIAGNOSTICS")
	private String triggeredDiagnostics;

	@Column(name = "DEVICE_IDENTIFIER")
	private String deviceIdentifier;
	
	@Column(name = "COMMENT")
	private String comment;
	

}
