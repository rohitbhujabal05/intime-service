package com.infinite.uptime.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Getter
@Setter
@Entity
@Table(name = "ATTACHMENT")
@JsonInclude(Include.NON_EMPTY)
public class Attachment implements Serializable {

	private static final long serialVersionUID = -48147982944389139L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "FILE_NAME", nullable = false)
	private String fileName;
	
	
	@Column(name = "FILE_PATH", nullable = false)
	private String filePath;

	@Column(name = "SIZE", nullable = false)
	private Long size;

	@Column(name = "TYPE", nullable = false)
	private String type;

	@Column(name = "CREATED", nullable = false)
	private LocalDateTime created;

	@Column(name = "UPDATED")
	private LocalDateTime updated;

}
