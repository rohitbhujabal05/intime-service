package com.infinite.uptime.model.request;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(Include.NON_EMPTY)
public class MachineDetailsUpdateRequest {

	private Long id;
    private Timestamp appointmentDate;
    private LocalDateTime createdDate;
    private Timestamp subscriptionStart;
    private Timestamp subscriptionEnd;
    private int speed;
    private int status;
    private String since;
    private String comment;

}
