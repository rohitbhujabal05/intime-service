package com.infinite.uptime.model.request;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@JsonInclude(Include.NON_EMPTY)
public class MachineDetailsRequest {

	 private String machineId;
	 private String machineName;
	 private String fault;
	 private Double healthScore;
	 private String recommendation;
	 private Timestamp appointmentDate;
	 private Long plantId;
	 private Long orgId;
	 private List<ServiceMachineRequest> serviceRequest;
	 private LocalDateTime createdDate;
	 private Timestamp subscriptionStart;
	 private Timestamp subscriptionEnd;
	 private int speed;
	 private String machineGroupName;
	 private int monitorId;
	 private String monitorName;
	 private int status;
	 private String since;
	 private Long alarms;
	 private String triggeredDiagnostics;
	 private String deviceIdentifier;
	 private String comment;
	
}
