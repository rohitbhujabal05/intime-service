package com.infinite.uptime.model.request;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@JsonInclude(Include.NON_EMPTY)
public class ServiceMachineRequest {

	private Long id;
	private String serviceName;
	private Long vendor;
	private LocalDateTime createdDate;
	
}
