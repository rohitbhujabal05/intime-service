package com.infinite.uptime.model.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(Include.NON_EMPTY)
public class AvailableUserUpdateRequest {
	
	private String status;
	private List<MachineDetailsUpdateRequest> machineDetailsRequest;

}
