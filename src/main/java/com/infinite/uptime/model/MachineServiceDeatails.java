package com.infinite.uptime.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Getter
@Setter
@Entity
@Table(name = "MACHINE_SERVICE_DETAILS")
@JsonInclude(Include.NON_EMPTY)
public class MachineServiceDeatails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	private Long id;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "MACHINE_DETAILS_ID")
    private ServiceReqMachineDetails serviceReqMachineDetails;
	
	@Column(name = "SERVICE_NAME")
	private String serviceName;

	@Column(name = "SERVICE_ID")
	private Long serviceId;

	@Column(name = "PRICE")
	private Double PRICE;
	
	@Column(name = "VENDOR_ID", nullable = false)
	private Long vendorId;
	

	@Column(name = "VENDOR_Name")
	private Long vendorName;

	@Column(name = "STATUS")
	private String status;

}
