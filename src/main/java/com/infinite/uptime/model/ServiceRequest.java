package com.infinite.uptime.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

@Getter
@Setter
@Entity
@Table(name = "SERVICE_REQ")
@JsonInclude(Include.NON_EMPTY)
public class ServiceRequest implements Serializable {
	private static final long serialVersionUID = -6189341734399375903L;

	public static String generateRandomDigits(int n) {
		int m = (int) Math.pow(10, n - 1);
		int result = m + new Random().nextInt(9 * m);
		System.out.println("\"IUSERV\" + result=============" + "IUSERV" + result);
		return "IUSERV" + result;
	}

	@Id
	@Column(name = "ID", nullable = false)
	private String id;

	@Column(name = "STATUS", nullable = false)
	private String status;

	@Column(name = "USER_ID", nullable = false)
	private Long userId;

	@Column(name = "CREATED_DATE", nullable = false)
	private LocalDateTime createdDate;

	@Column(name = "LAST_MODIFIED_DATE")
	private LocalDateTime lastModifiedDate;

	@JsonManagedReference
	@OneToMany(mappedBy = "serviceRequest", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ServiceReqMachineDetails> serviceReqMachineDetails;

	@OneToMany(cascade = CascadeType.ALL)

	@JoinTable(name = "ATTACHMENT_SER_REQ", joinColumns = {
			@JoinColumn(name = "SERVICE_REQ_ID") }, inverseJoinColumns = { @JoinColumn(name = "ATTACHMENT_ID") })
	private List<Attachment> attachments;

	public void setId() {
		this.id = generateRandomDigits(9);
	}

	public String getId() {
		return id;
	}

}
