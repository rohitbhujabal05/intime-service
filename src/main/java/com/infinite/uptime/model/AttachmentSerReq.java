package com.infinite.uptime.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ATTACHMENT_SER_REQ")
@JsonInclude(Include.NON_EMPTY)
@IdClass(AttachmentSerReqId.class)
public class AttachmentSerReq implements Serializable {

	
	private static final long serialVersionUID = 6214955735725485121L;
	@Id
	@Column(name = "ATTACHMENT_ID", nullable = false)
	private Long attacmentId;

	@Id
	@Column(name = "SERVICE_REQ_ID", nullable = false)
	@JsonIgnore
	private String serviceReqId;

}
