package com.infinite.uptime.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.infinite.uptime.util.IUtilConstants;


public class JLogManager {

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(JLogManager.class);

	/** The is debug. */
	private static boolean isDebug = logger.isDebugEnabled();

	/** The is info. */
	private static boolean isInfo = logger.isInfoEnabled();

	/**
	 * This method writes error logs.
	 *
	 * @param className  the class name
	 * @param methodName the method name
	 * @param msg        the error message
	 */
	public static void writeToAudit(String className, String methodName, String msg) {

		logger.error(className + IUtilConstants.DELEM_COLON + methodName + IUtilConstants.DELEM_SPACE + msg);

	}

	/**
	 * This method writes error logs.
	 *
	 * @param className  the class name
	 * @param methodName the method name
	 * @param t          the exception
	 */
	public static void writeToAudit(String className, String methodName, Throwable t) {

		logger.error(className + IUtilConstants.DELEM_COLON + methodName + IUtilConstants.DELEM_SPACE, t);

	}

	/**
	 * This method writes debug logs. className, methodName, msg
	 *
	 * @param className  the class name
	 * @param methodName the method name
	 * @param msg        the message
	 */
	public static void writeToTrace(String className, String methodName, String msg) {

		if (isDebug) {
			logger.debug(className + IUtilConstants.DELEM_COLON + methodName + IUtilConstants.DELEM_SPACE + msg);
		}

	}

	/**
	 * This method writes info logs. className, methodName, msg
	 *
	 * @param className  the class name
	 * @param methodName the method name
	 * @param msg        the message
	 */
	public static void writeToInfo(String className, String methodName, String msg) {

		if (isInfo) {
			logger.info(className + IUtilConstants.DELEM_COLON + methodName + IUtilConstants.DELEM_SPACE + msg);
		}

	}

	/**
	 * This method writes time take to execute service. className, methodName, msg
	 *
	 * @param className  the class name
	 * @param methodName the method name
	 * @param msg        the msg
	 * @param startTime  the start time
	 * @param endTime    the end time
	 */
	public static void writeTimeToTrace(String className, String methodName, String msg, long startTime, long endTime) {

		if (isInfo) {
			logger.info(className + IUtilConstants.DELEM_COLON + methodName + IUtilConstants.DELEM_SPACE + msg 
					+ IUtilConstants.DELEM_SPACE + (endTime - startTime) + IUtilConstants.DELEM_SPACE + IUtilConstants.UNIT_MS);
		}

	}

}
