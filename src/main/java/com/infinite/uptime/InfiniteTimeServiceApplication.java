package com.infinite.uptime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.infinite.uptime.model.FileStorageProperties;

@SuppressWarnings("deprecation")
@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class InfiniteTimeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfiniteTimeServiceApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**");	         
			}
		};
	}

}
