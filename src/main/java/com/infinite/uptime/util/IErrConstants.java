package com.infinite.uptime.util;

public interface IErrConstants {

	// HTTP REST API error constants
	public static final String HTTP000 = "HTTP000";
	public static final String HTTP001 = "HTTP001";
	public static final String HTTP002 = "HTTP002";
	public static final String HTTP003 = "HTTP003";
	public static final String HTTP004 = "HTTP004";

	// DB error constants
	public static final String DB000 = "DB000";

	// Business error constants
	public static final String IUT000 = "IUT000";
	public static final String IUT001 = "IUT001";
	public static final String IUT002 = "IUT002";
	public static final String IUT003 = "IUT003";
	public static final String IUT004 = "IUT004";
	public static final String IUT005 = "IUT005";
	public static final String IUT006 = "IUT006";
	public static final String IUT007 = "IUT007";
	public static final String IUT008 = "IUT008";
	public static final String IUT009 = "IUT009";
	public static final String IUT010 = "IUT010";
	public static final String IUT011 = "IUT011";

}
