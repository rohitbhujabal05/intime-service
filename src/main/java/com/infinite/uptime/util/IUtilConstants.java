package com.infinite.uptime.util;

public interface IUtilConstants {
	public static final String DELEM_COLON = ":";
	public static final String DELEM_SPACE = " ";
	public static final String UNIT_MS = "ms";
	
}
